/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.pkg2doparcial.paolacussi;

import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author Paola Soledad Cussi DNI: 35822347
 * En este proyecto se agregan 3 empleados para poder hacer pruevas de proyectos mas rapida
 * 
 */
public class Principal {

    public static void main(String[] args) {
        // Carga previa de empleados
        Empleado e1 = new Empleado("Juan Carlos", 1234567, "388512345", "juan@gmail.com");
        Empleado e2 = new Empleado("Carolina Perez", 3334444, "38855566", "carolina@gmail.com");
        Empleado e3 = new Empleado("Carla Juarez", 1112222, "38843434", "arla@gmail.com");

        int op = 0, opE = 0, opP = 0, dniEmp = -1;
        Empleado emp;
        EmpleadoDao empI = new EmpleadoDaoImp();
        empI.agregar(e1);
        empI.agregar(e2);
        empI.agregar(e3);

        Proyecto proy;
        ProyectoDao proyI = new ProyectoDaoImp();

        Scanner leer = new Scanner(System.in);
        try {
        while (op != 3) {
            
                System.out.println("\n*****  MENU  *****\n1) Empleados \n2) Proyectos \n3) Salir");
                op = leer.nextInt();
            
            switch (op) {
                case 1:
                    //*******  MENU EMPLEADOS *******
                    while (opE != 4) {
                        System.out.println("\n*******  EMPLEADOS *******\n1) Agregar Empleado \n2) Eliminar Empleado "
                                + "\n3) Ver todos los empleados \n4) Volver");
                        opE = leer.nextInt();
                        switch (opE) {
                            case 1://AGREGA UN EMPLEADO NUEVO
                                try {
                                    System.out.println("Ingrese numero de DNI");
                                    int dni = leer.nextInt();
                                    System.out.println("Ingrese el Nombre y Apellido");
                                    String nyp = leer.next();
                                    leer.nextLine();
                                    System.out.println("Ingrese numero de Telefono");
                                    String telefono = leer.next();
                                    leer.nextLine();
                                    System.out.println("Ingrese el email");
                                    String email = leer.next();
                                    leer.nextLine();
                                    emp = new Empleado(nyp, dni, telefono, email);
                                    empI.agregar(emp);
                                } catch (InputMismatchException ime) {
                                    System.out.println("Error");
                                }
                                break;
                            case 2://ELIMINA UN EMPLEADO POR EL DNI
                                try {
                                    System.out.println("Ingrese el DNI del Empleado que desea eliminar");
                                    int elim = leer.nextInt();
                                    empI.eliminar(elim);
                                } catch (InputMismatchException ime) {
                                    System.out.println("Error");
                                }
                                break;
                            case 3://LISTA LOS EMPLEADOS
                                System.out.println("***** EMPLEADOS *****");
                                
                                empI.listarO();
                                break;
                            case 4:// VOLVER 
                                break;
                            default:
                                System.out.println("Ingreso una opcion Incorrecta");
                                break;
                        }
                    }
                    break;
                case 2:
                    //*******  MENU PROYECTOS *******
                    while (opP != 8) {
                        System.out.println("\n*******  PROYECTOS ******* \n1) Agregar Nuevo Proyecto "
                                + "\n2) Eliminar un proyecto "
                                + "\n3) Ver todos los proyectos "
                                + "\n4) Agregar empleado a un proyecto "
                                + "\n5) Quitar un empleado de un proyecto "
                                + "\n6) Ver los datos de un proyecto y los empleados que participan "
                                + "\n7) Calcular el monto destinado a proyectos"
                                + "\n8) Volver");
                        opP = leer.nextInt();
                        switch (opP) {
                            case 1:// AGREGAR PROYECTO NUEVO
                                try {
                                    System.out.println("Ingrese Codigo de Proyecto");
                                    int id = leer.nextInt();
                                    System.out.println("Ingrese el Nombre del proyecto");
                                    String nom = leer.next();
                                    leer.nextLine();
                                    System.out.println("Ingrese monto destinado al Proyecto");
                                    int monto = leer.nextInt();
                                    leer.nextLine();
                                    proy = new Proyecto(id, nom, monto);
                                    proyI.agregar(proy);
                                } catch (IllegalArgumentException io) {
                                    System.out.println("Ingreso erroneo");
                                } catch (InputMismatchException ime) {
                                    System.out.println("Error");
                                }
                                break;
                            case 2://ELIMINAR PROYECTO POR CODIGO DE PROYECTO
                                try {
                                    System.out.println("Ingrese el Codigo del Proyecto que desea eliminar");
                                    int elim = leer.nextInt();
                                    proyI.eliminar(elim);
                                } catch (InputMismatchException ime) {
                                    System.out.println("Error");
                                }
                                break;
                            case 3://LISTAR TODOS LOS PROYECTOS
                                System.out.println("***** PROYECTOS *****");
                                proyI.listarP();
                                break;
                            case 4:// AGREGAR UN EMPLEADO A UN PROYECTO
                                try {
                                    proyI.listarP();
                                    System.out.println("Ingrese el Codigo del proyecto al que desea registar Empleados");
                                    int cod = leer.nextInt();
                                    leer.nextLine();
                                    empI.listarO();
                                    System.out.println("Ingrese el DNI del Empleado que desea agregar al proyecto");
                                    int dniP = leer.nextInt();
                                    leer.nextLine();
                                    proyI.agregarEmp(cod, empI.buscar(dniP));
                                } catch (InputMismatchException ime) {
                                    System.out.println("Error");
                                }
                                break;
                            case 5:// QUITAR UN EMPLEADO DE UN PROYECTO
                                try {
                                    proyI.listarP();
                                    System.out.println("Ingrese el Codigo del proyecto al que desea registar Empleados");
                                    int codE = leer.nextInt();
                                    leer.nextLine();
                                    proyI.listarE(codE);
                                    System.out.println("Ingrese el DNI del Empleado que desea agregar al proyecto");
                                    int dniE = leer.nextInt();
                                    proyI.quitarEmp(codE, dniE);
                                } catch (InputMismatchException ime) {
                                    System.out.println("Error");
                                }
                                break;
                            case 6:// VER DATOS DEL PROYECTO Y LOS EMPLEADOS QUE PARTICIPAN EN EL 
                                try {
                                    System.out.println("Ingrese el codigo de un proyecto");
                                    int codL = leer.nextInt();
                                    proyI.listarE(codL);
                                } catch (InputMismatchException ime) {
                                    System.out.println("Error");
                                }
                                break;
                            case 7://CALCULA EL MONTO TOTAL PARA EJECUTAR TODOS LOS PROYECTOS
                                proyI.calcularMonto();
                                break;
                            case 8://VOLVER
                                break;
                            default:
                                System.out.println("Ingreso una opcion Incorrecta");
                                break;
                        }
                    }
                    break;
                case 3:
                    System.out.println("FIN DEL PROGRAMA");
                    break;
                default:
                    System.out.println("Ingreso una opcion Incorrecta");
            }

        }
        } catch (InputMismatchException ime) {
                System.out.println("Ingreso una opcion Invalida");
            }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.pkg2doparcial.paolacussi;

/**
 *
 * @author Paola
 */
public class Empleado implements Comparable{
    public String nombre;    
    public int dni;
    public String telefono;
    public String email;

    public Empleado(String nombre, int dni,String telefono, String email) {
        this.nombre = nombre;        
        this.dni = dni;
        this.telefono = telefono;
        this.email = email;
    }
    
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int compareTo(Object o) {
        Empleado persona = (Empleado) o;
        int pers=0;
        if (persona.nombre.compareToIgnoreCase(persona.nombre) == 0) {
            if (persona.nombre.compareToIgnoreCase(persona.nombre) == 0) {
                pers = this.nombre.compareTo(persona.nombre);
            } else {

                pers = this.email.compareToIgnoreCase(persona.email);
            }
        } 
        return pers;
    }
           
}

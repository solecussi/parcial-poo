/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.pkg2doparcial.paolacussi;

/**
 *
 * @author Paola
 */
public interface ProyectoDao {

    public void agregar(Proyecto proy);

    public void eliminar(int id);

    public void listarP();
    
    public Proyecto buscar(int id);

    public void agregarEmp(int proy, Empleado emp);

    public void quitarEmp(int proy, int emp);
  
    public void listarE(int cod);

    public void calcularMonto();

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.pkg2doparcial.paolacussi;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Paola
 */
public class EmpleadoDaoImp implements EmpleadoDao {

    ArrayList<Empleado> empleados = new ArrayList();

    @Override
    public void agregar(Empleado emp) {
        empleados.add(emp);

    }

    @Override
    public void eliminar(int dni) {
        try {
            for (int i = 0; i <= empleados.size(); i++) {
                if (empleados.get(i).getDni() == dni) {
                    empleados.remove(empleados.get(i));
                }
            }
        } catch (ArrayIndexOutOfBoundsException aib) {
            System.out.println("El DNI del empleado no existe");
        }catch (IndexOutOfBoundsException ioe){
            System.out.println("Se elimino empleado");
        }
    }

    @Override
    //lista los empleados de forma desordenada
    public void listar(ArrayList emp) {

        for (Empleado empleado : empleados) {

            System.out.println("\n***** DATOS ******\nDNI: " + empleado.getDni() + "\nNombre y Apellido : "
                    + empleado.getNombre() + "\nTelefono: " + empleado.getTelefono()
                    + "\nEmail: " + empleado.getEmail());
        }
        System.out.println("\n******* TOTAL DE EMPLEADOS ********\nCantidad Total de Empleados: " + empleados.size());
    }

    @Override
    //busca un empleado y devuelve sus datos
    public Empleado buscar(int dni) {
        Empleado emp = null;
        for (Empleado empleado : empleados) {
            if (empleado.getDni() == dni) {
                emp = new Empleado(empleado.getNombre(), empleado.getDni(), empleado.getTelefono(), empleado.getEmail());
//                System.out.println("DNI: "+empleado.getDni()+"\nNombre y Apellido : "
//                    +empleado.getNombre()+"\nTelefono: "+empleado.getTelefono()
//                    +"Email: "+empleado.getEmail());
            }
        }
        return emp;
    }

    @Override
    //metodo para ordenar alfabericamente los nombres de los empleados
    public void listarO() {
        Collections.sort(empleados);
        listar(empleados);
    }

    @Override
    //verifica que empleados no estan en el proyecto
    public void verificarEmp(Proyecto proy) {
        try {
            for (int j = 0; j <= empleados.size(); j++) {                
                for (int i = 0; i <= proy.emp.size(); i++) {
                    if (proy.getEmp().get(i).dni != empleados.get(j).dni) {                       
                        System.out.println("\n***** DATOS ******\nDNI: " + empleados.get(j).getDni() 
                                + "\nNombre y Apellido : "+ empleados.get(j).getNombre() 
                                + "\nTelefono: " + empleados.get(j).getTelefono()
                                + "\nEmail: " + empleados.get(j).getEmail());
                    }
                }
            }

        } catch (ArrayIndexOutOfBoundsException aie) {
            System.out.println("Datos incorrectos");
        }
    }
}

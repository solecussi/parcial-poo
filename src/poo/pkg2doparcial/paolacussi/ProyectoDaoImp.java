/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.pkg2doparcial.paolacussi;

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Paola
 */
public class ProyectoDaoImp implements ProyectoDao {

    ArrayList<Empleado> empleados = new ArrayList(10);
    ArrayList<Proyecto> proyectos = new ArrayList();
    EmpleadoDao emple = new EmpleadoDaoImp();

    @Override
    //agrega un proyecto
    public void agregar(Proyecto proy) {
        proyectos.add(proy);
    }

    @Override
    //elimina un proyecto
    public void eliminar(int id) {

        try {
            for (int i = 0; i <= proyectos.size(); i++) {
                if (proyectos.get(i).getId() == id) {
                    proyectos.remove(proyectos.get(i));
                }
            }
        } catch (ArrayIndexOutOfBoundsException aib) {
            System.out.println("El numero de Proyecto no es correcto");
        }catch(IndexOutOfBoundsException ioe){
            System.out.println("Se elimino proyecto");
        }

    }

    @Override
    //lista los datos de los proyectos
    public void listarP() {
        for (Proyecto proyecto : proyectos) {
            System.out.println("\nCodigo: " + proyecto.getId() + "\nNombre: "
                    + proyecto.getNombre() + "\nMonto: " + proyecto.getPresupuesto());
        }
    }

    @Override
    public Proyecto buscar(int id) {
        Proyecto proy = null;
        for (Proyecto proyecto : proyectos) {
            if (proyecto.id == id) {
                proy = proyecto;
            }
        }
        return proy;
    }

    @Override
    //agrega un empleado a un proyecto
    public void agregarEmp(int proy, Empleado emp) {
        
        empleados.add(emp);
        for (Proyecto proyecto : proyectos) {

            if (proyecto.getId() == proy) {
                proyecto.setEmp(empleados);
            } else {
                System.out.println("Datos incorrectos");
            }
        }

    }

    @Override
    //eliminar empleado de un proyecto
    public void quitarEmp(int proy, int dni) {
        try {
            for (int j = 0; j <= proyectos.size(); j++) {
                if (proyectos.get(j).getId() == proy) {

                    for (int i = 0; i <= proyectos.get(j).getEmp().size(); i++) {
                        System.out.println(" dni " + proyectos.get(j).getEmp().get(i).getDni());

                        if (proyectos.get(j).getEmp().get(i).getDni() == dni) {
                            System.out.println("\nEmpleados: " + proyectos.get(j).getEmp().get(i).getNombre());
                            proyectos.get(j).getEmp().remove(proyectos.get(j).getEmp().get(i));

                        }else{
                            System.out.println("El empleado "+dni+" no existe");
                        }
                    }
                }else{
                    System.out.println("El proyecto "+proy+" no exixte");
                }
            }
        } catch (ArrayIndexOutOfBoundsException aie) {
            System.out.println("Datos incorrectos");
        }catch(IndexOutOfBoundsException iob){
            System.out.println("Se elimino empleado del proyecto");
        }
    }

    @Override
    //listar los proyectos y empleados que participan en el
    public void listarE(int cod) {
        try {
            for (Proyecto proyecto : proyectos) {
                if (proyecto.getId() == cod) {
                    System.out.println("\nCodigo: " + proyecto.getId() + "\nNombre: "
                            + proyecto.getNombre() + "\nMonto: " + proyecto.getPresupuesto() + "\nIntegrante del proyecto:");
                    for (Empleado emp : proyecto.getEmp()) {
                        System.out.println("\nNombre: " + emp.getNombre() + " DNI: " + emp.getDni());
                    }
                    System.out.println("Participan " + proyecto.getEmp().size() + " empleados en el proyecto");
                }else{
                    System.out.println("El proyecto "+cod+" no existe");
                }
            }
        } catch (ArrayIndexOutOfBoundsException aib) {
            System.out.println("El Proyecto con el codigo" + cod + " no exixte");
        }catch(NullPointerException ne){
            System.out.println("Sin empleados");
        }

    }

    @Override
    //Calcular el total de presupuesto para todos los proyectos
    public void calcularMonto() {
        double total = 0, p;
        for (Proyecto proyecto : proyectos) {
            total = (proyecto.getPresupuesto() + total);
        }
        System.out.println("Para el desarrollo de todos los proyectos e necesario un presupuesto de " + total);
    }

}

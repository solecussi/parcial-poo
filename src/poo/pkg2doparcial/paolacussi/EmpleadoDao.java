/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.pkg2doparcial.paolacussi;

import java.util.ArrayList;

/**
 *
 * @author Paola
 */
public interface EmpleadoDao {

    public void agregar(Empleado emp);

    public void eliminar(int dni);

    public void listar(ArrayList emp);

    public Empleado buscar(int dni);

    public void listarO();
    
    public void verificarEmp(Proyecto proy);
}

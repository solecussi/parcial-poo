/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.pkg2doparcial.paolacussi;

import java.util.ArrayList;

/**
 *
 * @author Paola
 */
public class Proyecto {
    public int id;
    public String nombre;
    public double presupuesto;
    public ArrayList<Empleado> emp;

    public Proyecto(int id, String nombre, double presupuesto) {
        this.id = id;
        this.nombre = nombre;
        this.presupuesto = presupuesto;                
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(double presupuesto) {
        this.presupuesto = presupuesto;
    }

    public ArrayList<Empleado> getEmp() {
        return emp;
    }

    public void setEmp(ArrayList<Empleado> emp) {
        this.emp = emp;
    }
    
}
